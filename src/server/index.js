// https://www.npmjs.com/package/convert-csv-to-json

let csvToJson = require('convert-csv-to-json');

let fileInputNameDeliveries = 'deliveries.csv';
let fileInputNameMatches = 'matches.csv';


let matches = csvToJson.fieldDelimiter(",").getJsonFromCsv(fileInputNameMatches);
let deliveries = csvToJson.fieldDelimiter(",").getJsonFromCsv(fileInputNameDeliveries);

// console.log(matches);
// console.log(deliveries);


function matchesPlayedPerYear(acc, current) {
    if (acc[current["season"]]) {
        acc[current["season"]] = acc[current["season"]] + 1
    }
    else {
        acc[current["season"]] = 1;
    }
    return acc;
}
const result = matches.reduce(matchesPlayedPerYear, {})

//console.log(result)

// ----------------------------------------------------------------------------------------------------------------
function matchesWonByTeamPerYear(acc, current) {
    if (!acc[current["season"]]) {
        acc[current["season"]] = {}
        acc[current["season"]][current["winner"]] = 1
    }
    else {
        if (!acc[current["season"]][current["winner"]]) {
            acc[current["season"]][current["winner"]] = 1
        } else {
            acc[current["season"]][current["winner"]]++
        }
    }
    return acc;
}
const result2 = matches.reduce(matchesWonByTeamPerYear, {})
//console.log(result2)

// 2nd problem anotherway
// function matchesWonByTeamPerYear(acc, current) {
//     if (!acc[current["winner"]]) {
//         acc[current["winner"]] = {}
//         acc[current["winner"]][current["season"]] = 1
//     }
//     else {
//         if (!acc[current["winner"]][current["season"]]) {
//             acc[current["winner"]][current["season"]] = 1
//         } else {
//             acc[current["winner"]][current["season"]]++
//         }
//     }
//     return acc;
// }
// const result2a = matches.reduce(matchesWonByTeamPerYear, {})

//console.log(result2a)


// 3.Extra runs conceded per team in the year 2016.

function extraRunsConcededPerTeam(acc, current) {
    if (!acc[current["bowling_team"]]) {
            acc[current["bowling_team"]] = 0   //[current["extra_runs"]] = 0;
    }
        else {
            if(acc[current["bowling_team"]]){
                acc[current["bowling_team"]][current["extra_runs"]]++
            }
            
        }

    
    return acc;
}

const result3 = deliveries.reduce(extraRunsConcededPerTeam, {})
//console.log(result3)

// ------------------------------------------------------------------------------------------------------------------

function extraRuns(matches,deliveries){
    let extraRuns = {};
    let iddata = []
    matches.filter(element => {
        if(element["season"] == 2016){
            iddata.push(element.id)
        }
    })
    deliveries.map(element => {
        if(element["match_id"] in iddata){
            let extra = Number(element["extra_runs"]);
            if(extraRuns.hasOwnProperty(element["bowling_team"])){
                extraRuns[element.bowling_team] += extra
            } else{
                extraRuns[element.bowling_team] = extra;
            }
        }
    })
    return extraRuns;
}
 console.log(extraRuns(matches,deliveries))


 // 4. Top 10 economical bowlers in the year 2015

 function economicalBowlers(matches,deliveries){
    let iddata2 = []
    let bowlers = {};
    matches.filter(element => {
        if(element["season"] == 2015){
            iddata2.push(element.id)
        }
    })
    deliveries.map(element => {
        if(element["match_id"] in iddata2){
            let runs = Number(element["total_runs"]);
            if(bowlers.hasOwnProperty(element["bowler"])){
                bowlers[element.bowler] += runs
            } else{
                bowlers[element.bowler] = runs;
            }
        }
    })
    return bowlers
 }

 const result10 = economicalBowlers(matches,deliveries);
 //console.log(result10)